import org.apache.http.*;
import org.apache.http.message.*;
import org.apache.http.HttpEntity;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.apache.commons.io.IOUtils;
import java.util.*;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.*;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.cookie.*;
import org.apache.http.client.*;
import org.apache.http.impl.client.*;

import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import org.apache.poi.ss.usermodel.*;
import org.apache.poi.hssf.usermodel.*;
import java.io.*;
//Calendar.get(Calendar.DAY/YEAR/WHATEVER);
//http://docs.oracle.com/javase/6/docs/api/java/util/Date.html

public class StockBot
{
    public boolean isLoggedIn;
    private CloseableHttpClient httpClient;
    private CookieStore cookieStore;
    public String gameName;
    public HashMap<String, Stock> myStocks;
    private Logger log;
    private ArrayList<Hist> hist;
    public StockBot()
    {
        this("redditchallenge2014");
    }
    public StockBot(String myGameName)
    {
        log = Logger.getLogger("MyLog");
        isLoggedIn = false;
        cookieStore = new BasicCookieStore();
        httpClient = HttpClients.custom()
        .setDefaultCookieStore(cookieStore)
        .build();
        gameName = myGameName;
        hist = new ArrayList<Hist>();
    }

    public boolean buyStock(String ticker, int quantity) throws Exception
    {
        log.finest("Called");
        if (!isLoggedIn) 
        {
            log.warning("isLoggedIn = false, returning");
            return false;
        }
        if (myStocks.get(ticker.toUpperCase())==null)
        {
            String tempFuid = getFuid(ticker.toUpperCase());
            if  (tempFuid==null) { log.warning("tempFuid==null"); return false; }
            Stock tempStock = new Stock();
            tempStock.fuid = tempFuid;
            tempStock.ticker = ticker.toUpperCase();
            myStocks.put(ticker.toUpperCase(), tempStock);
        }
        Stock stock = myStocks.get(ticker.toUpperCase());
        if (stock.shares<0) { log.warning("if returning false!"); return false; }
        String body = "[{\"Fuid\":\""+stock.fuid+"\",\"Shares\":\""+quantity+"\",\"Type\":\"Buy\",\"Term\":\"Cancelled\"}]";
        String response = post(body);
        System.out.println("Buy Stock "+ticker.toUpperCase()+": "+response);
        log.info("Buy Stock "+ticker.toUpperCase()+": "+response);
        stock.shares += quantity;
        hist.add(new Hist(ticker.toUpperCase(), "Buy", quantity, getStockPrice(ticker)));
        return true;
    }

    public boolean sellStock(String tick, int quantity) throws Exception
    {
        log.finest("Called");
        String ticker = tick.toUpperCase();
        if (!isLoggedIn) 
        {
            log.warning("isLoggedIn = false, returning");
            return false;
        }
        if (myStocks.get(ticker)==null) return false;
        Stock stock = myStocks.get(ticker);
        if (stock.shares<=0) { log.warning("if returning false!"); return false; }
        String body = "[{\"Fuid\":\""+stock.fuid+"\",\"Shares\":\""+quantity+"\",\"Type\":\"Sell\",\"Term\":\"Cancelled\"}]";
        String response = post(body);
        System.out.println("Sell Stock "+ticker+": "+response);
        log.info("Sell Stock "+ticker+": "+response);
        stock.shares -= quantity;
        hist.add(new Hist(ticker, "Sell", quantity, getStockPrice(ticker)));
        return true;
    }

    public boolean shortStock(String ticker, int quantity) throws Exception
    {
        log.finest("Called");
        if (!isLoggedIn) 
        {
            log.warning("isLoggedIn = false, returning");
            return false;
        }
        if (myStocks.get(ticker.toUpperCase())==null)
        {
            String tempFuid = getFuid(ticker.toUpperCase());
            if  (tempFuid==null) { log.warning("tempFuid==null"); return false; }
            Stock tempStock = new Stock();
            tempStock.fuid = tempFuid;
            tempStock.ticker = ticker.toUpperCase();
            myStocks.put(ticker.toUpperCase(), tempStock);
        }
        Stock stock = myStocks.get(ticker.toUpperCase());
        if (stock.shares>0) { log.warning("if returning false!"); return false; }
        String body = "[{\"Fuid\":\""+stock.fuid+"\",\"Shares\":\""+quantity+"\",\"Type\":\"Short\",\"Term\":\"Cancelled\"}]";
        String response = post(body);
        System.out.println("Short Stock "+ticker.toUpperCase()+": "+response);
        log.info("Short Stock "+ticker.toUpperCase()+": "+response);
        stock.shares -= quantity;
        hist.add(new Hist(ticker, "Short", quantity, getStockPrice(ticker)));
        return true;
        /*
        if (!isLoggedIn) return false;
        if (myStocks.get(ticker)==null) return false;
        Stock stock = myStocks.get(ticker);
        if (stock.shares>0) return false;
        String body = "[{\"Fuid\":\""+stock.fuid+"\",\"Shares\":\""+quantity+"\",\"Type\":\"Short\",\"Term\":\"Cancelled\"}]";
        String response = post(body);
        System.out.println("Short Stock "+ticker+": "+response);
        return true;*/
    }

    public boolean coverStock(String tick, int quantity) throws Exception
    {
        log.finest("Called");
        String ticker = tick.toUpperCase();
        if (!isLoggedIn) 
        {
            log.warning("isLoggedIn = false, returning");
            return false;
        }
        if (myStocks.get(ticker)==null) return false;
        Stock stock = myStocks.get(ticker);
        if (stock.shares>=0) { log.warning("if returning false!"); return false; }
        String body = "[{\"Fuid\":\""+stock.fuid+"\",\"Shares\":\""+quantity+"\",\"Type\":\"Cover\",\"Term\":\"Cancelled\"}]";
        String response = post(body);
        System.out.println("Cover Stock "+ticker+": "+response);
        log.info("Cover Stock "+ticker+": "+response);
        stock.shares += quantity;
        hist.add(new Hist(ticker, "Buy2Cover", quantity, getStockPrice(ticker)));
        return true;
    }

    public double getStockPrice(String tick) throws Exception
    {
        String ticker = tick.toUpperCase();
        if (!isLoggedIn) 
        {
            log.warning("isLoggedIn = false, returning");
            return -1;
        }
        log.info("getting trade page");
        String tradeP = get("http://www.marketwatch.com/game/redditchallenge2014/trade");
        log.finer("done getting trade page");
        String body = "search="+ticker+"&view=grid&partial=true";
        HttpPost httpPost = new HttpPost("http://www.marketwatch.com/game/"+gameName+"/trade?week=1");
        httpPost.addHeader("Request", "POST /game/"+gameName+"/trade?week=1 HTTP/1.1");
        httpPost.addHeader("Accept", "*/*");
        httpPost.addHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
        httpPost.addHeader("Referer", "http://www.marketwatch.com/game/"+gameName+"/trade");
        httpPost.addHeader("X-Requested-With", "XMLHttpRequest");
        httpPost.addHeader("Accept-Encoding", "gzip, deflate");
        httpPost.addHeader("User-Agent", "Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.1; Trident/6.0)");
        httpPost.addHeader("Host", "www.marketwatch.com");
        httpPost.addHeader("DNT", "1");
        httpPost.addHeader("Connection", "Keep-Alive");
        httpPost.addHeader("Cache-Control", "no-cache");
        httpPost.setEntity(new StringEntity(body));

        CloseableHttpResponse response = httpClient.execute(httpPost);
        String content = "CONTENT_PLACEHOLDER";
        try {
            //System.out.println(response.getStatusLine());
            log.finer(response.getStatusLine().toString());
            HttpEntity entity = response.getEntity();
            content = IOUtils.toString(entity.getContent(), "UTF-8");
            EntityUtils.consume(entity);
        } finally {
            response.close();
        }
        String res = content;
        //System.out.println("res: "+res);
        int start = res.indexOf("<div data-symbol=");
        if (start==-1) {
            log.warning("Stock "+ticker+" not found, returning -2");
            log.finer("Content:\n"+content);
            return -2;
        }
        if (res.indexOf("<div data-symbol=\""+ticker+"\" class=\"chip disabled\"><!--[if lte IE 8]>")!=-1)
        {
            log.warning("Couldn't get it?? returning -3 res:\n"+res);
            return -3;
        }
        if (res.substring(start, res.indexOf(">",start)).split("\"").length<7) { log.warning("GOING TO BREAK, res:\n"+res); }
        double price = Double.parseDouble(res.substring(start, res.indexOf(">",start)).split("\"")[7]);
        log.info("Got price for "+ticker);
        log.finer("price="+price);
        return price;
    }

    public String getFuid(String ticker) throws Exception
    {
        if (!isLoggedIn)
        {
            log.warning("isLoggedIn = false, returning");
            return null;
        }
        String body = "search="+ticker+"&view=grid&partial=true";
        HttpPost httpPost = new HttpPost("http://www.marketwatch.com/game/"+gameName+"/trade?week=1");
        httpPost.addHeader("Request", "POST /game/"+gameName+"/trade?week=1 HTTP/1.1");
        httpPost.addHeader("Accept", "*/*");
        httpPost.addHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
        httpPost.addHeader("X-Requested-With", "XMLHttpRequest");
        httpPost.addHeader("Accept-Encoding", "gzip, deflate");
        httpPost.addHeader("User-Agent", "Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.1; Trident/6.0)");
        httpPost.addHeader("Host", "www.marketwatch.com");
        httpPost.addHeader("DNT", "1");
        httpPost.addHeader("Connection", "Keep-Alive");
        httpPost.addHeader("Cache-Control", "no-cache");
        httpPost.setEntity(new StringEntity(body));

        CloseableHttpResponse response = httpClient.execute(httpPost);
        String res = "CONTENT_PLACEHOLDER";
        try {
            System.out.println(response.getStatusLine());
            log.finer(response.getStatusLine().toString());
            HttpEntity entity = response.getEntity();
            res = IOUtils.toString(entity.getContent(), "UTF-8");
            EntityUtils.consume(entity);
        } finally {
            response.close();
        }

        int start = res.indexOf("<div data-symbol=");
        if (start==-1) {
            log.warning("Stock "+ticker+" not found");
            log.finer("res:\n"+res);
            return null;
        }
        String rtn = res.substring(start, res.indexOf(">",start)).split("\"")[1];
        log.info("Got fuid for "+ticker);
        log.finer("fuid="+rtn);
        return rtn;
    }

    public void setGame(String gn) throws Exception
    {
        gameName = gn;
        log.fine("Set game: "+gn);
        if (isLoggedIn) loadGame();
    }

    private String post(String body) throws Exception
    {
        log.finest("Called");
        if (!isLoggedIn) 
        {
            log.warning("isLoggedIn = false, returning");
            return "NotLoggedIn";
        }

        HttpPost httpPost = new HttpPost("http://www.marketwatch.com/game/"+gameName+"/trade/submitorder?week=1");
        httpPost.addHeader("Request", "POST /game/"+gameName+"/trade/submitorder?week=1 HTTP/1.1");
        httpPost.addHeader("Accept", "application/json, text/javascript, */*; q=0.01");
        httpPost.addHeader("Content-Type", "application/json; charset=UTF-8");
        httpPost.addHeader("X-Requested-With", "XMLHttpRequest");
        httpPost.addHeader("Accept-Encoding", "gzip, deflate");
        httpPost.addHeader("User-Agent", "Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.1; Trident/6.0)");
        httpPost.addHeader("Host", "www.marketwatch.com");
        httpPost.addHeader("DNT", "1");
        httpPost.addHeader("Connection", "Keep-Alive");
        httpPost.addHeader("Cache-Control", "no-cache");
        httpPost.setEntity(new StringEntity(body));
        log.fine("body:"+body);
        CloseableHttpResponse response = httpClient.execute(httpPost);
        String content = "CONTENT_PLACEHOLDER";
        try {
            System.out.println("post:"+response.getStatusLine()+" | "+body);
            log.finer("post:"+response.getStatusLine()+" | "+body);
            HttpEntity entity = response.getEntity();
            content = IOUtils.toString(entity.getContent(), "UTF-8");
            EntityUtils.consume(entity);
        } finally {
            response.close();
        }
        return content;
    }

    public String get(String url) throws Exception
    {
        log.finest("Called");
        HttpGet httpget = new HttpGet(url);
        CloseableHttpResponse response = httpClient.execute(httpget);
        //response = httpClient.execute(httpget);
        String content = "CONTENT_PLACEHOLDER";
        log.finer(response.getStatusLine().toString());
        try {
            HttpEntity entity = response.getEntity();
            //System.out.println("----------------------------------------");
            //System.out.println(response.getStatusLine());
            if (entity != null) {
                //StringWriter writer = new StringWriter();
                //System.out.println("Response content length: " + entity.getContentLength());
                content = IOUtils.toString(entity.getContent(), "UTF-8");
                //String myString = IOUtils.toString(myInputStream, "UTF-8");
            }
            EntityUtils.consume(entity);
        } finally {
            response.close();
        }
        return content;
    }

    private void loadGame() throws Exception
    {
        String page = get("http://www.marketwatch.com/game/"+gameName+"/portfolio/Holdings?sort=PlayersHolding&amp;descending=True");
        myStocks = new HashMap<String, Stock>();
        int indx = 0;
        while (page.indexOf("<tr data-symbol=\"",indx)!=-1)
        {
            int endinfo = page.indexOf(">",page.indexOf("<tr data-symbol=\"",indx));
            String tinfo = page.substring(page.indexOf("<tr data-symbol=\"",indx),endinfo);
            tinfo = tinfo.replace("\"","").replace(" ","=");
            //System.out.println("tinfo: "+tinfo);
            String tinfoa[] = tinfo.split("=");
            Stock stock = new Stock();
            stock.fuid = tinfoa[2];
            stock.ticker = tinfoa[4];
            //System.out.println("[10]="+tinfoa[10]+" | [11]="+tinfoa[11]+" | [12]="+tinfoa[12]+" | [13]="+tinfoa[13]);
            stock.shares = (tinfoa[10].equalsIgnoreCase("Buy")) ? Double.parseDouble(tinfoa[12]) : -1.0 * Double.parseDouble(tinfoa[12]);
            myStocks.put(tinfoa[4].toUpperCase(), stock);
            indx = endinfo;
        }
        /*System.out.println("OWNED STOCKS:");
        for (Stock s : myStocks)
        {
        System.out.println(s);
        }*/
        System.out.println("myStocks:\n"+myStocks);
        System.out.println("***\n*\tDone loading game\n***");
        //    http://www.marketwatch.com/game/redditchallenge2014/portfolio/Holdings?sort=PlayersHolding&amp;descending=True
    }

    public void login(String user, String pass) throws Exception
    {
        if (isLoggedIn) {
            log.warning("Already logged in (?), returning");
            return;
        }
        HttpGet httpget = new HttpGet("http://id.marketwatch.com/access/50eb2d087826a77e5d000001/latest/login_standalone.html");
        System.out.println("Sending login page request");
        CloseableHttpResponse response = httpClient.execute(httpget);
        try {
            HttpEntity entity = response.getEntity();
            log.finer(response.getStatusLine().toString());

            //System.out.println("----------------------------------------");
            System.out.println(response.getStatusLine());
            if (entity != null) {
                //StringWriter writer = new StringWriter();
                //System.out.println("Response content length: " + entity.getContentLength());
                //System.out.println(IOUtils.toString(entity.getContent(), "UTF-8"));
                //String myString = IOUtils.toString(myInputStream, "UTF-8");
            }
            EntityUtils.consume(entity);
        } finally {
            response.close();
        }
        //Send login POST
        String JSON_STRING = "{\"username\":\""+user+"\",\"password\":\""+pass+"\",\"url\":\"https://id.marketwatch.com/access/50eb2d087826a77e5d000001/latest/login_reload.html\",\"template\":\"default\",\"realm\":\"default\",\"savelogin\":\"true\"}";
        HttpPost httpPost = new HttpPost("http://id.marketwatch.com/auth/submitlogin.json");
        httpPost.addHeader("Request", "POST /auth/submitlogin.json HTTP/1.1");
        httpPost.addHeader("Accept", "application/json, text/javascript, */*; q=0.01");
        httpPost.addHeader("Content-Type", "application/json; charset=UTF-8");
        httpPost.addHeader("X-HTTP-Method-Override", "POST");
        httpPost.addHeader("X-Requested-With", "XMLHttpRequest");
        httpPost.addHeader("Accept-Encoding", "gzip, deflate");
        httpPost.addHeader("User-Agent", "Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.1; Trident/6.0)");
        httpPost.addHeader("Host", "id.marketwatch.com");
        httpPost.addHeader("DNT", "1");
        httpPost.addHeader("Connection", "Keep-Alive");
        httpPost.addHeader("Cache-Control", "no-cache");
        httpPost.setEntity(new StringEntity(JSON_STRING));
        response = httpClient.execute(httpPost);
        String newurl = null;
        try {
            System.out.println(response.getStatusLine());
            HttpEntity entity2 = response.getEntity();
            String temp = IOUtils.toString(entity2.getContent(), "UTF-8");
            System.out.println(temp); //Now get the new url
            newurl = temp.substring(temp.indexOf("http"),temp.indexOf("\"",temp.indexOf("http")));
            //System.out.println("URL: "+newurl);
            EntityUtils.consume(entity2);
        } finally {
            response.close();
        }

        //Send a GET to the new url so we get the session cookie
        get(newurl);
        /*
        httpget = new HttpGet(newurl);
        //System.out.println("Sending login cookie request:\n*\t" + httpget.getRequestLine());
        response = httpClient.execute(httpget);
        try {
        HttpEntity entity = response.getEntity();

        System.out.println("----------------------------------------");
        System.out.println(response.getStatusLine());
        if (entity != null) {
        //StringWriter writer = new StringWriter();
        System.out.println("Response content length: " + entity.getContentLength());
        System.out.println(IOUtils.toString(entity.getContent(), "UTF-8"));
        //String myString = IOUtils.toString(myInputStream, "UTF-8");
        }
        EntityUtils.consume(entity);
        } finally {
        response.close();
        }*/
        isLoggedIn = true;
        System.out.println("***\n*\tDone with login\n***");
        if (gameName!=null) loadGame();
    }

    public Workbook getHistWorkbook() //Finish this
    {
        Workbook wb = new HSSFWorkbook(); //http://poi.apache.org/spreadsheet/quick-guide.html
        Sheet sheet = wb.createSheet("Sheet One");
        CreationHelper createHelper = wb.getCreationHelper();
        Row r = sheet.createRow(0);
        r.createCell(0).setCellValue(
            createHelper.createRichTextString("Symbol"));
        r.createCell(1).setCellValue(
            createHelper.createRichTextString("Date"));
        r.createCell(2).setCellValue(
            createHelper.createRichTextString("Type"));
        r.createCell(3).setCellValue(
            createHelper.createRichTextString("Shares"));
        r.createCell(4).setCellValue(
            createHelper.createRichTextString("Cost Per Share"));
        r.createCell(5).setCellValue(
            createHelper.createRichTextString("Total Cost"));

        int counter = 1;
        Row row = null;
        for (Hist entry : hist)
        {
            row = sheet.createRow(counter);
            row.createCell(0).setCellValue(
                createHelper.createRichTextString(entry.getTicker()));
            row.createCell(1).setCellValue(
                createHelper.createRichTextString(entry.getDate()));
            row.createCell(2).setCellValue(
                createHelper.createRichTextString(entry.getType()));
            row.createCell(3).setCellValue(entry.getQuantity());
            row.createCell(4).setCellValue(entry.getCost());
            row.createCell(5).setCellValue(entry.getCost()*entry.getQuantity());
            counter++;
        }

        return wb;
    }

    public static void writeWorkbook(Workbook wb) throws Exception
    {
        File nameCheck = new File("BotHistWorkbook.xls");
        String name = "Workbooks/BotHistWorkbook.xls";
        int myCounter = 1;
        while (nameCheck.exists())
        {
            name = "Workbooks/BotHistWorkbook"+myCounter+".xls";
            nameCheck = new File(name);
            myCounter++;
        }

        FileOutputStream fileOut = new FileOutputStream(name);
        wb.write(fileOut);
        fileOut.close();
    }

    public static void writeWorkbook(StockBot bot) throws Exception
    {
        StockBot.writeWorkbook(bot.getHistWorkbook());
    }

}