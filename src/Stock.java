public class Stock
{
    public String ticker, fuid;
    public double shares;
    public Stock()
    {
        ticker = "";
        shares = 0;
        fuid = "";
    }
    public Stock(String n)
    {
        ticker = new String(n);
        shares = 0;
        fuid = "";
    }
    
    public String toString()
    {
        String rtn = "["+ticker+" | Type: ";
        if (shares<0) rtn += "Short";
        if (shares>0) rtn += "Buy";
        if (shares==0) rtn += "---";
        rtn += " | Shares: " + Math.abs(shares) + " | Fuid: " + fuid + "]";
        return rtn;
    }
    
    //Fuid names <-- Might be unnecessary
    //public static final String MSFT = "STOCK-NSQ-MSFT";
    //public static final String AAPL = 
}