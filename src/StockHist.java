import java.util.*;
import java.text.*;
public class StockHist
{
    public String ticker, type;
    public double exec;
    public int shares;
    private Date date;
    public String orderDate, transDate;
    public String companyName;
    public StockHist() {}
    public StockHist(String tick, String typ, String orderDat, String transDat, int share, double exe)
    {
        ticker = tick;
        type = typ;
        shares = share;
        exec = exe;
        orderDate = orderDat;
        transDate = transDat;
        companyName = "PLACEHOLDER";
    }
    /*public String getDate() { return backupString; }
    public String getType() { return type; }
    public double getsharesity() {  return sharesity; }
    public String getTicker() { return ticker; }
    public double getCost() { return cost; } */
    public String toString()
    {
        String rtn = "[";
        rtn += ticker + "| " + orderDate + "/" + transDate + " | " + type + " " + shares + " | $" 
            + exec + "]";
        return rtn;
    }
}