import java.util.*;
import java.text.*;
public class Hist
{
    private String ticker, type;
    private double quantity, cost;
    private Date date;
    private String backupString;
    public Hist(String tick, String typ, int quant, double cst)
    {
        ticker = tick;
        type = typ;
        quantity = quant;
        cost = cst;
        DateFormat df = new SimpleDateFormat("dd/MM/yy HH:mm");
        date = new Date();
        backupString = df.format(new Date());
    }
    public String getDate() { return backupString; }
    public String getType() { return type; }
    public double getQuantity() {  return quantity; }
    public String getTicker() { return ticker; }
    public double getCost() { return cost; }
    public String toString()
    {
        DateFormat df = new SimpleDateFormat("dd/MM/yy HH:mm");
        String rtn = "[";
        rtn += backupString + "| " + type + " " + quantity + " | " 
            + ticker + " | $" + cost + "/share]";
        return rtn;
    }
}