**# README #**

This is a kind of API for MarketWatch's Virtual Stock Exchange Game (See [http://www.marketwatch.com/game/](http://www.marketwatch.com/game/)).  
I made it my Senior year in High School for my AP Economics class, in which we had a game going on MarketWatch.  
A friend and I came up with the idea to make a program to play the game for us, and thus StockBot was formed.  
I've kept it untouched from the end of my Senior year.  
  
This was never a project I intended for anybody else to use other than my friend that I was on a team with, and so it is quite messy.  
I'm considering forking it and then cleaning it up, and making it a bit easier to read.  
I've compiled it and it was able to start up and run, so it seems to be in working order but I have not done extensive testing.  
I've included some documentation below, but if it isn't clear enough you can message me and I'll be happy to help.
---  
For now, here's a small guide.  
One thing to note is that pretty much *every* method throws Exception.  
*I am aware of how bad this is, but at the time it was more convenient, and I was just typing out code quickly.*  

*Create an instance and login like this:*  
  

```
#!java

StockBot bot = new StockBot("GameNameHere"); // Get the game name from the url Ex: http://www.marketwatch.com/game/GameNameHere/  
bot.login("email", "password");  
```
  
After logging in you can call the rest of the methods, most of which are pretty self-explanatory.  

Each of these methods returns false if they don't work (ticker doesn't exist, don't have shares to sell, etc.)  
```
#!java

public boolean buyStock(String ticker, int quantity) throws Exception { ... }
public boolean sellStock(String tick, int quantity) throws Exception { ... }
public boolean shortStock(String ticker, int quantity) throws Exception { ... }
public boolean coverStock(String tick, int quantity) throws Exception { ... }

```
The rest of the methods include:  

```
#!java

// Most of these will return -1 (If double) or null (if String) if the method fails for some reason.
public double getStockPrice(String tick) throws Exception { ... }
public String getFuid(String ticker) throws Exception { ... }
public void setGame(String gn) throws Exception { ... }
private void loadGame() throws Exception { ... }
public void login(String user, String pass) throws Exception { ... }

```

The bot also keeps track of the transactions it makes, and has the ability to write them to a workbook.  
It uses Apache POI to write to an Workbook that can be saved as a .xls file. *(There is a note that says "Finish this", but it appears to be in working order)*

```
#!java

public Workbook getHistWorkbook() { ... } //Finish this (Done?)
public static void writeWorkbook(Workbook wb) throws Exception { ... }
public static void writeWorkbook(StockBot bot) throws Exception { ... }

```